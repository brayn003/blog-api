<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
// use Illuminate\Http\Request;
use App\Articles;

class ArticleController extends Controller
{

		public function list()
		{
			// echo "hello";
				return Articles::leftJoin('users', 'users.id', '=', 'articles.user_id')
												 ->paginate(5,['articles.*','users.name AS user_name']);
												 // ->(15);
		}

		public function show($id)
		{
				return Articles::find($id);
		}

		public function listSlugs()
		{
				// echo "string";
				return Articles::get(['slug']);
		}

		public function ShowBySlug($slug)
		{
				return Articles::where('slug','=',$slug)
												->first();
		}

		public function listIds()
		{
				return Articles::get(['id']);
		}
}
