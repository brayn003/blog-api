<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Users::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Articles::class, function (Faker\Generator $faker) {
    // static $password;
		$title = $faker->words(6, true);
		$slug = implode("-",explode(" ",strtolower($title)));
    return [
        // 'name' => $faker->name,
        'user_id' => $faker->numberBetween(1,30),
        'slug' => $slug,
        'title' => $title,
        'feature_image' => $faker->imageUrl(800, 600),
        'excerpt' => $faker->paragraph(8),
        'body' => $faker->paragraphs(8,true)
    ];
});
