<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        // $users = [
        // 	['name'=>'Amy','password'=>Hash::make('amy'),'email'=>'amy@abc.com']
        // ];

        // DB::table('users')->insert($users);
        factory(App\Users::class, 50)->create();
    }
}
