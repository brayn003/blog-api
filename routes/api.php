<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');

Route::get('/article', ['middleware' => 'cors', 'uses' => 'ArticleController@list']);
Route::get('/article/{id}', ['middleware' => 'cors', 'uses' => 'ArticleController@show']);
Route::get('/article-slugs', ['middleware' => 'cors', 'uses' => 'ArticleController@listSlugs']);
Route::get('/article-slugs/{slug}', ['middleware' => 'cors', 'uses' => 'ArticleController@showBySlug']);
Route::get('/article-ids/', ['middleware' => 'cors', 'uses' => 'ArticleController@listIds']);